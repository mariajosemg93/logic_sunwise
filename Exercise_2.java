import java.util.Scanner;

public class ejercicio2 {
    
    //Converts to AUC BC: 754 - year & AD: year + 753
    public static int yearAUC(String AUC){
        if (AUC.indexOf("A") != -1){
            String[] array = AUC.split("A");
            String decimal = array[0];
            return Integer.parseInt(decimal) + 753;
        }
        else {
            String[] array = AUC.split("B");
            String decimal = array[0];
            return 754 - Integer.parseInt(decimal);
        }
    }
    
    //Converts to Roman Numeric System 
    //Retrieved from: https://algorithms.tutorialhorizon.com/convert-integer-to-roman/
    public static int integerToRoman(int num) {

        int[] values = {1000,900,500,400,100,90,50,40,10,9,5,4,1};
        String[] romanLiterals = {"M","CM","D","CD","C","XC","L","XL","X","IX","V","IV","I"};

        StringBuilder roman = new StringBuilder();

        for(int i=0;i<values.length;i++) {
            while(num >= values[i]) {
                num -= values[i];
                roman.append(romanLiterals[i]);
            }
        }
        return roman.toString().length();
    }
    
    public static void main(String args[]) {
        
        //Establish year range
        Scanner scan = new Scanner(System.in);
        String range = scan.nextLine();
        
        //String split "-"
        String[] yearRange = range.split("-");
        String fromYear = yearRange[0];
        String toYear = yearRange[1];
        
        //Convert BC-AD to AUC
        int fromAUC = yearAUC(fromYear);
        int toAUC = yearAUC(toYear);
        
        //Compares length of Roman number to find the greatest
        int max = 0;
        for(int i = fromAUC; i <= toAUC ; i++) {
            if (integerToRoman(i) > max){
                max = integerToRoman(i);
            }
        }
        System.out.println(max);
        
    } 
    
}