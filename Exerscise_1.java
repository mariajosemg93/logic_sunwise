import java.util.Scanner;

public class ejercicio1 {
    public static void main(String args[]) {
        
        Scanner scan = new Scanner(System.in);
        //number of test cases
        int T = scan.nextInt(); 
        
        for (int i = 0; i < T; i++){
            //number of rows
            int N = scan.nextInt();
            //number of columns
            int M = scan.nextInt();
           
           //if N is greater than M, then 2 outputs: Up or Down
            if (N > M){
                //if M is an even number, then output is Up, if not it's Down
                if (M % 2 == 0){ 
                    System.out.println("U"); 
                }
                else System.out.println("D");
            }
            
            //if M is greater or equal than N, then 2 outputs: Right or Left
            else if (M >= N){
                if (N % 2 == 0){
                    //if N is an even number, then output is Left, if not it's Right
                    System.out.println("L");
                }
                else System.out.println("R");
            }
        }
    }
}